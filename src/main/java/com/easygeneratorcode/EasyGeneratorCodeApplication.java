package com.easygeneratorcode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EasyGeneratorCodeApplication {

    public static void main(String[] args) {
        SpringApplication.run(EasyGeneratorCodeApplication.class, args);
    }

}
